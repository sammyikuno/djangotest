from django.urls import path
from . import views

app_name = 'soudn'

urlpatterns = [
        path('', views.index, name='index'),
        path('csv_import', views.csv_import, name='csv_import')
]