from django.shortcuts import render
from django.http import HttpResponse
from .models import FileUpload

import csv
import io
import pandas as pd

# Create your views here.
def index(request):
    """
    トップページ
    """
    file_obj = FileUpload.objects.all()
    context = {
            'file_obj': file_obj,
    }
    return render(request, 'soudn/index.html', context)


def csv_import(request):

    # resultに渡す文字列セット

    debstrtext = "const char* const sndDbgNameTbl[DATA_LIST_NUM] = {"+ '\r\n'
    text = ""
    text +=  "/***********************************************"+ '\r\n'
    text +=  ""+ '\r\n'
    text +=  "    　　サウンド単独制御指定用データ"+ '\r\n'
    text +=  ""+ '\r\n'
    text +=  "***********************************************/"+ '\r\n'
    text +=  ""+ '\r\n'
    text +=  "#include ""snd_def.h"""+ '\r\n'
    text +=  "//  制御情報、"+ '\r\n'
    text +=  "//      ﾁｬﾈﾙ,ﾌﾚｰｽﾞ,ﾎﾞﾘｭｰﾑ,ﾎﾞﾘｭｰﾑ遷移,左右ﾊﾟﾝ,左右ﾊﾟﾝ遷移,上下ﾊﾟﾝ,上下ﾊﾟﾝ遷移,(ﾀﾞﾐｰ),再生方法"+ '\r\n'
    text +=  "//                      又は、"+ '\r\n'
    text +=  "//  制御情報（未使用）"+ '\r\n'
    text +=  "//      ｼｰｹﾝｻ,ｺｰﾄﾞ,ﾀｲﾏ上位,ﾀｲﾏ下位,ｵﾌﾄﾘｶﾞ,ｽﾄｯﾌﾟ上位,ｽﾄｯﾌﾟ下位,(ﾀﾞﾐｰ),(ﾀﾞﾐｰ),起動方法"+ '\r\n'
    text +=  "//                      又は、"+ '\r\n'
    text +=  "//  制御情報（未使用）"+ '\r\n'
    text +=  "//      (ﾀﾞﾐｰ),ｼﾝﾌﾟﾙｱｸｾｽｺｰﾄﾞ,(ﾀﾞﾐｰ),(ﾀﾞﾐｰ),(ﾀﾞﾐｰ),(ﾀﾞﾐｰ),(ﾀﾞﾐｰ),(ﾀﾞﾐｰ),(ﾀﾞﾐｰ),(ﾀﾞﾐｰ),"+ '\r\n'
    text +=  "#ifdef  SNDCTL_PHS"+ '\r\n'
    text +=  "#undef  SNDCTL_PHS"+ '\r\n'
    text +=  "#endif"+ '\r\n'
    text +=  "#define SNDCTL_PHS(no,ch,vol,pn1,pn2,lp,rp)                 \\"+ '\r\n'
    text +=  "    {PHS_CTL|PHS_FNUM|PHS_VOL|PHS_PAN1|PHS_PAN2|PHS_PLAY,   \\"+ '\r\n'
    text +=  "        ch,no,vol,0,pn1,0,pn2,0,(rp),lp},    "+ '\r\n'
    text +=  ""+ '\r\n'
    text +=  "#ifndef DATA_LIST_NUM"+ '\r\n'
    text +=  "#define DATA_LIST_NUM    (8192)"+ '\r\n'
    text +=  "#endif"+ '\r\n'
    text +=  ""+ '\r\n'
    text +=  "#ifdef DEBUG"+ '\r\n'
    text +=  "const char* PC_SNDCTL_EXPORTER_VER = ""v.1.0.3"";"+ '\r\n'
    text +=  "#endif"+ '\r\n'
    text +=  ""+ '\r\n'
    text +=  "const SNDCTL TblSoundSelect[DATA_LIST_NUM] = " + '\r\n'
    text +=  "{" + '\r\n'
    text +=  "//         ﾌﾚｰｽﾞ番号   ﾁｬﾝﾈﾙ    ﾎﾞﾘｭｰﾑ 左右ﾊﾟﾝ 上下ﾊﾟﾝ  SorM?  ループ" + '\r\n'

    if 'xlsm' in request.FILES:
        # 指定エクセル読み込み部
        buf = request.FILES['xlsm'].read()
        data = pd.read_excel(buf, sheet_name='snd (サウンドから)', index_col=0, usecols = "A:I", skiprows=2, dtype=str)
        counter = 0
        # 走査の処理
        for row in data.itertuples():

            # A列が空白ならbreakさせる
            counter += 1
            if(counter == 8191):
                break

            stemonostr =  ""
            if(('S' in str(row[6])) == True):
                stemonostr =  "SND_STE"
            else:
                stemonostr =  "SND_MON"

            loopstr =  ""
            if(('ON' in str(row[3])) == True):
                loopstr =  "1"
            else:
                loopstr =  "0"

            text += "SNDCTL_PHS(" + str(row[0]) + ", " + "CH_" + str(row[4]) + ", "  + str(row[3]) + ", " + str(row[7]) + ", " + str(row[8]) + ", " +stemonostr + ", " +  loopstr + ")     " + "// " + str(row[0]) + ": " + str(row[1]) + '\r\n'
            debstrtext += "    \"" + str(row[1]) + "\"," + '\r\n'

    text +=  "};" + '\r\n' + '\r\n' + '\r\n'
    debstrtext += "};" + '\r\n'
    text += debstrtext
    text +=  "unsigned int sndDbgNameMax = sizeof(sndDbgNameTbl) / sizeof(char*);" + '\r\n'
    
    params = {
        'table' : text
    }

    #return render(request, 'soudn/result.html', params)
    response = HttpResponse(text, content_type="text/plain")
    response["Content-Disposition"] = "attachment; filename=TBL_CmdSndSet.c"
    return response

    